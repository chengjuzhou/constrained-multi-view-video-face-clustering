clear;
clc;

restoredefaultpath;

addpath('scsr/');
addpath('code_coregspectral/')

% Data sets
DATASET_TYPE = {'NH','TBBTS06E12','YouTube_6'};


% Constraints type
%CONSTRAINT_TYPE = {no,mlink,clink}:
%0 no use,1 use
CONSTRAINT_TYPE.no = 1;
CONSTRAINT_TYPE.mlink = 1;
CONSTRAINT_TYPE.clink = 1;
CONSTRAINT_TYPE.all = 1;
%correnpodning flag is 1,the constraints will be used.The order is not
%changed
CONSTRAINT_TYPE_FLAGS = {CONSTRAINT_TYPE.no,CONSTRAINT_TYPE.mlink,CONSTRAINT_TYPE.clink,CONSTRAINT_TYPE.all};
CONSTRAINT_TYPE_FLAGS_STR = {'no','mlink','clink','all'};


% Feature type
FEATURE_TYPE = {'lbp','gabor','gray'};

%%MULTI_VIEW_FLAG = {1,1,0,0},its length must be same to the length of FEATURE_TYPE
%if the corresponded flag is 1,it will be used in multi-view
%For instance: FEATURE_TYPE = {'gray','gabor','hog','lbp'};
%              MULTI_VIEW_FLAG = {1,1,0,1}
% then the gray gabor and lbp will be selected in multi-view
MULTI_VIEW_FLAG = {1,1,1};

%% Link ratio
mlink_ratio = 10; % for combination feature directly
clink_ratio = 0;  %not change


%% Groundtruth: Corresponding to the DATASET_TYPE
GT = {[18 11 20 13 14],...
    [76 59 131 18 19 11 38 27 6],...
    [ 6 6 6 6 6 6 6 6]};

%% The sampled num of each track.
% Note that, if the number of faces in track is less than the num,
% all of the faces in this track will be used.
% Corresponding to the DATASET_TYPE
SAMPLED_NUM = {[3 3],[5,5],[10 10]};%

% Run the Methods: Single Multi-view
SINGLE_VIEW = 1;
MULTI_VIEW = 1;


for dataset_index = 1:1:1 %length(DATASET_TYPE)
    dataset = DATASET_TYPE{dataset_index};
    sample_num = SAMPLED_NUM{dataset_index};
    
    %load the info
    load(['data/' dataset '/' dataset '_info.mat']);
    feature_vecs = {};
    com_vec = [];
    % Load the feature
    for i = 1:1:length(FEATURE_TYPE)
        load(['data/' dataset '/' dataset '_' FEATURE_TYPE{i} '.mat']);
        % gray norm
        if strcmp(FEATURE_TYPE{i},'gray')
            vecs = norm_gray(vecs);
        end
        feature_vecs{i} = vecs;        
    end
    clear vec gray_vecs gabor_vecs hog_vecs lbp_vecs com_vec;
    for sample_num_i = sample_num(1):1:sample_num(2)
        %build the sample_index
        [sample_index,num_each_track,sample_label,gt_sample_label,num_each_class] = track_sample2(labels,sample_num_i);
        [constraint_ml,constraint_cl,constraint_no] = build_constraint_matrix(sample_label,cannot_link_pairs);
        constraint_SSC = {};
        %not use constraint
        if CONSTRAINT_TYPE.no == 1
            constraint_SSC{1} = constraint_no;
        end
        %use mlink
        if CONSTRAINT_TYPE.mlink == 1
            constraint_SSC{2} = constraint_ml + constraint_no;
        end
        %use clink
        if CONSTRAINT_TYPE.clink == 1
            constraint_SSC{3} = abs(constraint_cl) + constraint_no;
        end
        % use all link
        if CONSTRAINT_TYPE.all == 1
            constraint_SSC{4} = constraint_ml + abs(constraint_cl) + constraint_no;
        end
        num_clusters = length(GT{dataset_index});
        
        % build the W
        [ mid_W ] = build_mid_W( feature_vecs,sample_index,dataset,sample_num_i,FEATURE_TYPE,constraint_SSC,num_clusters,CONSTRAINT_TYPE_FLAGS,CONSTRAINT_TYPE_FLAGS_STR);
        
        % Constraints,fibal_W used in single-view,final_W2 used in multi-view
        [ final_W,final_W2 ] = build_final_W( mid_W,constraint_ml,mlink_ratio,constraint_cl,clink_ratio,constraint_no,CONSTRAINT_TYPE_FLAGS,CONSTRAINT_TYPE_FLAGS_STR);
        
        % Clustering
        for it_num_i = 1:1:1
            
            %single view
            if SINGLE_VIEW
                [ result_SINGLE ] = single_view_method( final_W, sample_num_i,num_each_class,num_each_track, num_clusters,gt_sample_label,FEATURE_TYPE,DATASET_TYPE{dataset_index},CONSTRAINT_TYPE_FLAGS,CONSTRAINT_TYPE_FLAGS_STR,constraint_ml,constraint_cl);
                sample_num_result{sample_num_i}.SINGLE = result_SINGLE;
                clear result_SINGLE;
            end
            
            % Multi-view
            if MULTI_VIEW
                [ result_MULTI ] = multi_view_method( final_W2,MULTI_VIEW_FLAG,sample_num_i,constraint_ml,constraint_cl,gt_sample_label, num_clusters,num_each_track,num_each_class,FEATURE_TYPE,DATASET_TYPE{dataset_index},CONSTRAINT_TYPE_FLAGS,CONSTRAINT_TYPE_FLAGS_STR);
                sample_num_result{sample_num_i}.MULTI = result_MULTI;
            end
            
        end %end it_num
        
    end %end sampl_num
    
end


fprintf('Done \n');
