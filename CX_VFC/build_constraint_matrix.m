function [ constraint_ml,constraint_cl,constraint_no ] = build_constraint_matrix( label,cannot_link_pairs)
%BUILD_CNSTRAINTS Summary of this function goes here
%   Detailed explanation goes here


% the constraints
classes = unique(label);
mlink_pair = [];
clink_pair = [];

for i = 1:1:length(classes)
    track_index = find(label == classes(i));
    
    for j = 1:1:length(track_index)
        for k = j+1:1:length(track_index)
            mlink_pair = [mlink_pair; track_index(j) track_index(k)];
        end 
    end 
end 

cannot_tracks = cannot_link_pairs;

for i = 1:1:size(cannot_tracks,1)
    track1_index = find(label == cannot_tracks(i,1));
    track2_index = find(label == cannot_tracks(i,2));
    
    for j = 1:1:length(track1_index)
        for k = 1:1:length(track2_index)
            clink_pair = [clink_pair;track1_index(j) track2_index(k)];
        end
    end
end

% construct constraint using in sparse representation
%constraint_SSC = zeros(length(label),length(label));
constraint_ml = zeros(length(label),length(label));
constraint_cl = zeros(length(label),length(label));


    for i = 1:1:size(mlink_pair,1)
        left = mlink_pair(i,1);
        right = mlink_pair(i,2);
        %constraint_SSC(left,right) = 1;
        %constraint_SSC(right,left) = 1;
        
        constraint_ml(left,right) = 1;
        constraint_ml(right,left) = 1;
    end


    for i = 1:1:size(clink_pair,1)
        left = clink_pair(i,1);
        right = clink_pair(i,2);
        %constraint_SSC(left,right) = 1;
        %constraint_SSC(right,left) = 1;
        
        constraint_cl(left,right) = -1;
        constraint_cl(right,left) = -1;
    end



% use constraints
% constraint_SSC = constraint_SSC + eye(length(label));
 %constraint_ml = constraint_ml; 
 constraint_no = eye(length(label));


end

