function [ final_W,final_W2 ] = build_final_W( mid_W,constraint_ml,mlink_ratio,constraint_cl,clink_ratio,constraint_no,CONSTRAINT_TYPE_FLAGS,CONSTRAINT_TYPE_FLAGS_STR)
%BUILD_FINAL_W Summary of this function goes here
%   Detailed explanation goes here

final_W = {};
for mid_W_i = 1:1:length(mid_W)
    if CONSTRAINT_TYPE_FLAGS{1}  == 1
        final_W{mid_W_i}{1} = mid_W{mid_W_i}{1};% + mlink_ratio*constraint_ml + clink_ratio*constraint_cl;%+ mlink_ratio*constraint_no;% + mlink_ratio*constraint_ml + clink_ratio*constraint_cl;
        final_W2{mid_W_i}{1} = mid_W{mid_W_i}{1};
    end
    if CONSTRAINT_TYPE_FLAGS{2} == 1
        final_W{mid_W_i}{2} = mid_W{mid_W_i}{2} + mlink_ratio*constraint_ml; %+ mlink_ratio*constraint_no;% + mlink_ratio*constraint_ml + clink_ratio*constraint_cl;
        final_W2{mid_W_i}{2} = mid_W{mid_W_i}{2};
    end
    if CONSTRAINT_TYPE_FLAGS{3} == 1
        final_W{mid_W_i}{3} = mid_W{mid_W_i}{3} + clink_ratio*constraint_cl;% + mlink_ratio*constraint_no;% + mlink_ratio*constraint_ml + clink_ratio*constraint_cl;
        final_W2{mid_W_i}{3} = mid_W{mid_W_i}{3};
    end
    if CONSTRAINT_TYPE_FLAGS{4} == 1
        final_W{mid_W_i}{4} = mid_W{mid_W_i}{4} + mlink_ratio*constraint_ml + clink_ratio*constraint_cl;% +  mlink_ratio*constraint_no;% + mlink_ratio*constraint_ml + clink_ratio*constraint_cl;
        final_W2{mid_W_i}{4} = mid_W{mid_W_i}{4};
    end
    
    %final_W2{mid_W_i} = mid_W{mid_W_i};
end
end

