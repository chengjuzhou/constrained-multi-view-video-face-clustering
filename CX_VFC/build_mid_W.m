function [ mid_W ] = build_mid_W( feature_vecs,sample_index,dataset,sample_num_i,FEATURE_TYPE,constraint_SSC,num_clusters,CONSTRAINT_TYPE_FLAGS,CONSTRAINT_TYPE_FLAGS_STR)
%BUILD_MID_W Summary of this function goes here
%   Detailed explanation goes here

%% build the W
alpha = 20; % default
% alpha = 10;
r = 0; outlier = true; rho = 1;
mid_W = {};
%num_clusters = length(GT{dataset_index});
for feature_vecs_i = 1:1:length(feature_vecs)
    for CONSTRAINT_TYPE_FLAGS_i = 1:1:length(CONSTRAINT_TYPE_FLAGS)
        if CONSTRAINT_TYPE_FLAGS{CONSTRAINT_TYPE_FLAGS_i} == 0
            continue
        end
        save_name_suf = CONSTRAINT_TYPE_FLAGS_STR{CONSTRAINT_TYPE_FLAGS_i};
        constraint_SSC_temp = constraint_SSC{CONSTRAINT_TYPE_FLAGS_i};
        if exist(fullfile(['data/' dataset '/mid_W/' num2str(sample_num_i) '-' FEATURE_TYPE{feature_vecs_i} '_' save_name_suf '_' num2str(alpha) '_' '.mat']),'file')
            load(fullfile(['data/' dataset '/mid_W/' num2str(sample_num_i) '-' FEATURE_TYPE{feature_vecs_i} '_' save_name_suf '_' num2str(alpha) '_' '.mat']));
        else
            
            
            data = feature_vecs{feature_vecs_i};
            data = data(sample_index,:);
            if feature_vecs_i ~= 3
                [re_pc,re_score,re_latent,re_tsquare] = princomp(data);
                data = re_score(:,1:1536);
            end
            data = data';
            
            % data = double(data);
            [W] = SSR(data,r,constraint_SSC_temp,alpha,outlier,rho,num_clusters);
            save(fullfile(['data/' dataset '/mid_W/' num2str(sample_num_i) '-' FEATURE_TYPE{feature_vecs_i} '_' save_name_suf '_' num2str(alpha) '_' '.mat']),'W');
        end
        mid_W{feature_vecs_i}{CONSTRAINT_TYPE_FLAGS_i} = W;
    end
end

end

