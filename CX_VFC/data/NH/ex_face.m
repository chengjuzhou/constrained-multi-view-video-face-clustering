load NH_gray.mat;
load NH_info.mat;

! rm -r faces
mkdir('faces');
for i = 1:1:size(vecs,1)
    img = vecs(i,:);
    img = uint8(reshape(img,[50 40]));
    if exist(['faces/' num2str(labels(i,2)) '/'],'dir')==0
        mkdir(['faces/' num2str(labels(i,2)) '/']);
    end
    
    imwrite(img,fullfile(['faces/' num2str(labels(i,2)) '/' num2str(labels(i,2)) '_' num2str(vecs(i,1)) '_' num2str(i) '.jpg']));
end
