load NH_gray.mat;
load NH_info.mat;

! rm -r faces_one
mkdir('faces_one');
for i = 1:1:size(vecs,1)
    img = vecs(i,:);
    img = uint8(reshape(img,[50 40]));
    
    
    imwrite(img,fullfile(['faces_one/' num2str(labels(i,2)) '.jpg']));
end