
function [ result_MULTI ] = multi_view_method( final_W2_in,MULTI_VIEW_FLAG,sample_num_i,constraint_ml,constraint_cl,gt_sample_label, num_clusters,num_each_track,num_each_class,FEATURE_TYPE,DATASET_TYPE_now,CONSTRAINT_TYPE_FLAGS,CONSTRAINT_TYPE_FLAGS_STR)
%MULTI_VIEW_METHOD Summary of this function goes here
%   Detailed explanation goes here

addpath('code_coregspectral/')
result_MULTI = {};
for CONSTRAINT_TYPE_FLAGS_i = 1:1:length(CONSTRAINT_TYPE_FLAGS)
    final_W2 = {};
    for final_W2_in_i = 1:1:length(final_W2_in)
        final_W2{final_W2_in_i} = final_W2_in{final_W2_in_i}{CONSTRAINT_TYPE_FLAGS_i};
    end
    
    % selected feature used in multi_view
    final_W2_index = [];
    for i = 1:1:length(MULTI_VIEW_FLAG)
        if MULTI_VIEW_FLAG{i} == 1
            final_W2_index = [ final_W2_index i];
        end
    end
    
    if strcmp(DATASET_TYPE_now, 'NH')
        pa_mv_1 = 2;
        pa_mv_2 = 1;
        pa_mv_3 = 0.0001;
        % best feature in the first order which can be obtained from the single
        % view
        final_W2 = final_W2([3 2 1]);
    end
    
    epson = -0.0000000001;
    constraint_ml = constraint_ml + epson;
    constraint_cl = constraint_cl + epson;
    
    
    predict_label = NewConstrained_spectral_pairwise_multview(final_W2(final_W2_index),num_clusters,pa_mv_1,pa_mv_2,pa_mv_3,constraint_ml,constraint_cl,10);
    %predict_label = constrained_spectral_pairwise_multview(final_W,num_clusters,5,10);
    predict_label_final = norm_predict_label(predict_label,num_each_track);
    [confusion_matrix,trace_max]=confusion_compute(predict_label_final, num_each_class);
    
    
    %%%%%%%%%%%%%%%%%%%
    [F,P,R] = compute_f(gt_sample_label,predict_label);
    [A nmi avgent] = compute_nmi(gt_sample_label,predict_label);
    [AR,RI,MI,HI]=RandIndex(gt_sample_label,predict_label);
    %%%%%%%%%%%%%%%%%%
    fprintf('===================================== \n');
    fprintf('Method is: MULTI_VIEW \n');
    fprintf('constraint is: %s \n',CONSTRAINT_TYPE_FLAGS_STR{CONSTRAINT_TYPE_FLAGS_i});
    fprintf('dataset is: %s \n',DATASET_TYPE_now);
    fprintf('MtV: feature is: ');
    multi_view_features = [];
    for i = 1:1:length(MULTI_VIEW_FLAG)
        if MULTI_VIEW_FLAG{i} == 1
            fprintf('%s ',FEATURE_TYPE{i});
            multi_view_features = [multi_view_features ' ' FEATURE_TYPE{i}];
        end
    end
    fprintf('\nMtV:sample num is : %d \n',sample_num_i);
    fprintf('MtV:Accuracy  is : %f \n',trace_max/sum(num_each_class));
    fprintf('F P R NMI RI is : %f;%f;%f;%f;%f \n',F,P,R,nmi,RI);
    fprintf('===================================== \n');
    
    result_MULTI{1}{CONSTRAINT_TYPE_FLAGS_i}.accu_per = trace_max/sum(num_each_class);
    result_MULTI{1}{CONSTRAINT_TYPE_FLAGS_i}.accu_num = trace_max;
    result_MULTI{1}{CONSTRAINT_TYPE_FLAGS_i}.F = F;
    result_MULTI{1}{CONSTRAINT_TYPE_FLAGS_i}.P = P;
    result_MULTI{1}{CONSTRAINT_TYPE_FLAGS_i}.R = R;
    result_MULTI{1}{CONSTRAINT_TYPE_FLAGS_i}.nmi = nmi;
    result_MULTI{1}{CONSTRAINT_TYPE_FLAGS_i}.RI = RI;
    result_MULTI{1}{CONSTRAINT_TYPE_FLAGS_i}.FEATURE = multi_view_features;
    result_MULTI{1}{CONSTRAINT_TYPE_FLAGS_i}.CONSTRAINT_TYPE = CONSTRAINT_TYPE_FLAGS_STR{CONSTRAINT_TYPE_FLAGS_i};
end


end

