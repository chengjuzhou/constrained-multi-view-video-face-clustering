function [ result_SINGLE ] = single_view_method( final_W, sample_num_i,num_each_class,num_each_track, num_clusters,gt_sample_label,FEATURE_TYPE,DATASET_TYPE_now,CONSTRAINT_TYPE_FLAGS,CONSTRAINT_TYPE_FLAGS_STR,constraint_ml,constraint_cl)
%SINGLE_VIEW_METHOD Summary of this function goes here
%   Detailed explanation goes here

result_SINGLE = {};
for final_W_i = 1:1:length(final_W) 
    for CONSTRAINT_TYPE_FLAGS_i = 1:1:length(CONSTRAINT_TYPE_FLAGS)
        if CONSTRAINT_TYPE_FLAGS{CONSTRAINT_TYPE_FLAGS_i} == 0
            continue
        end
        alpha = 1;
        beta = 0;
        if CONSTRAINT_TYPE_FLAGS_i == 1
            ml = constraint_ml*0;
            cl = constraint_cl*0;
         end
        if CONSTRAINT_TYPE_FLAGS_i == 2
            ml = constraint_ml;
            cl = constraint_cl*0;
         end
         if CONSTRAINT_TYPE_FLAGS_i == 3
            ml = constraint_ml*0;
            cl = constraint_cl;
         end
         if CONSTRAINT_TYPE_FLAGS_i == 4
            ml = constraint_ml;
            cl = constraint_cl;
         end
         
        %predict_label = CSpectralClustering(final_W{final_W_i}{1},ml,cl,alpha,beta,num_clusters);
        predict_label = SpectralClustering(final_W{final_W_i}{CONSTRAINT_TYPE_FLAGS_i},num_clusters);
        predict_label_final = norm_predict_label(predict_label,num_each_track);
        [confusion_matrix,trace_max]=confusion_compute(predict_label_final, num_each_class);   
        %%%%%%%%%%%%%%%%%%%
        [F,P,R] = compute_f(gt_sample_label,predict_label);
        [A nmi avgent] = compute_nmi(gt_sample_label,predict_label);
        [AR,RI,MI,HI]=RandIndex(gt_sample_label,predict_label);
        %%%%%%%%%%%%%%%%%%
        fprintf('===================================== \n');
        fprintf('Method is: SINGLE_VIEW \n');
        fprintf('constraint type is: %s \n',CONSTRAINT_TYPE_FLAGS_STR{CONSTRAINT_TYPE_FLAGS_i});
        fprintf('dataset is: %s \n',DATASET_TYPE_now);
        fprintf('feature is: %s \n',FEATURE_TYPE{final_W_i});
        fprintf('sample num is : %d \n',sample_num_i);
        fprintf('Accuracy  is : %f \n',trace_max/sum(num_each_class)); 
        fprintf('F P R NMI RI is : %f;%f;%f;%f;%f \n',F,P,R,nmi,RI);
        fprintf('===================================== \n');
        result_SINGLE{final_W_i}{CONSTRAINT_TYPE_FLAGS_i}.accu_per = trace_max/sum(num_each_class);
        result_SINGLE{final_W_i}{CONSTRAINT_TYPE_FLAGS_i}.accu_num = trace_max;
        result_SINGLE{final_W_i}{CONSTRAINT_TYPE_FLAGS_i}.F = F;
        result_SINGLE{final_W_i}{CONSTRAINT_TYPE_FLAGS_i}.P = P;
        result_SINGLE{final_W_i}{CONSTRAINT_TYPE_FLAGS_i}.R = R;
        result_SINGLE{final_W_i}{CONSTRAINT_TYPE_FLAGS_i}.nmi = nmi;
        result_SINGLE{final_W_i}{CONSTRAINT_TYPE_FLAGS_i}.RI = RI;
        result_SINGLE{final_W_i}{CONSTRAINT_TYPE_FLAGS_i}.FEATURE = FEATURE_TYPE{final_W_i};
        result_SINGLE{final_W_i}{CONSTRAINT_TYPE_FLAGS_i}.CONSTRAINT_TYPE = CONSTRAINT_TYPE_FLAGS_STR{CONSTRAINT_TYPE_FLAGS_i};
end

end

