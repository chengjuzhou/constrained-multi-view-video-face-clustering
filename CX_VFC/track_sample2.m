function [ sample_index, num_each_track,sample_label,gt_sample_label,num_each_class]=track_sample_tip_review(labels,sample_num)

% for no use err data
% tmp_index = find(labels(:,1)==10);
% labels = labels(1:tmp_index(1)-1,:);




label = labels(:,2);
classes = unique(label);



tmp_truth = labels(:,1);

sample_index = [];
num_each_track = [];

num_cluster = length(unique(tmp_truth));

num_each_class = zeros(num_cluster,1);
track_face_num_thresh = 20;
for i = 1:1:length(classes)
    track_index = find(label== classes(i));
    
%     if length(track_index) < 20
%         continue;
%     end
    num_each_class(tmp_truth(track_index(1))) = num_each_class(tmp_truth(track_index(1))) + 1;
    
    if length(track_index) <= sample_num
        sample_index = [sample_index; track_index];
        num_each_track = [num_each_track; length(track_index)];
        continue;
    else
        sample_step = floor(length(track_index) ./ sample_num);
        
        for j = 1:1:sample_num
            sample_index = [sample_index;track_index(sample_step * j)];
        end
        num_each_track = [num_each_track; sample_num];
    end
end
sample_label = label(sample_index,:);
gt_sample_label = labels(sample_index,1);

end
