This is the code of the work described in the following paper:

Constrained Multi-View Video Face Clustering

Cao Xiaochun, Changqing Zhang, Chengju Zhou, Huazhu Fu, Hassan Foroosh

IEEE Transactions on Image Processing

The code includes two different versions of video face clustering algorithm: 
CS_VFC(for single view) and CM_VFC(for multiple view)

Usage: run 'CX_VFC_demo.m'

Please contact Chengju ZHOU (chengjuzhou@outlook.com) for questions about the code.